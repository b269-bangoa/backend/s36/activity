//Contains instructions on HOW your API will perform its intended tasks
//All operations needed will be placed here

const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskID) => {
	return Task.findByIdAndRemove(taskID).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return "Deleted task"
		}
	})
};

module.exports.getSpecificTask = (taskID) => {
	return Task.findById(taskID).then(result => {
		return result;
	});
};

module.exports.changeStatus = (taskID) => {
	return Task.findById(taskID).then((updateStatus, err) => {
		updateStatus = Task({
			name: updateStatus.name,
			status: "completed"
		});
		if(err){
			console.log(err);
			return false;
		} else {
			return updateStatus;
		}
	})
};

