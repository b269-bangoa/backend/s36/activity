//Setup dependencies

const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

//Server setup

const app = express();
const port = 3003;

//Middlewares

app.use(express.json());
app.use(express.urlencoded({extended: true}));


//Database connection
mongoose.connect ("mongodb+srv://yanab:admin123@zuitt-bottcamp.muceymo.mongodb.net/s36-activity?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log(`Now connected to cloud database`));

app.use("/tasks", taskRoute);


app.listen(port, () => console.log(`Server running at port ${port}`));